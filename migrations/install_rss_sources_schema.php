<?php

namespace athc\rssreader\migrations;

class install_rss_sources_schema extends \phpbb\db\migration\migration
{
    public function effectively_installed()
    {
        return $this->db_tools->sql_table_exists($this->table_prefix . 'rss_sources');
    }

    static public function depends_on()
    {
        return array('\phpbb\db\migration\data\v31x\v314');
    }

    public function update_schema()
    {
        return array(
            'add_tables'		=> array(
                $this->table_prefix . 'rss_sources'	=> array(
                    'COLUMNS'		=> array(
                        'id'			=> array('UINT', null, 'auto_increment'),
                        'url'			=> array('VCHAR:255', ''),
                        'label'			=> array('VCHAR:255', ''),
                        'num'			=> array('UINT', 5),
                        'active'		=> array('USINT', 1),
                    ),
                    'PRIMARY_KEY'	=> 'id',
                ),
            ),
        );
    }

    public function revert_schema()
    {
        return array(
            'drop_tables'		=> array(
                $this->table_prefix . 'rss_sources',
            ),
        );
    }
}
