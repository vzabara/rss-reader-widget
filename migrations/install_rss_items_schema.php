<?php

namespace athc\rssreader\migrations;

class install_rss_items_schema extends \phpbb\db\migration\migration
{
    public function effectively_installed()
    {
        return $this->db_tools->sql_table_exists($this->table_prefix . 'rss_items');
    }

    static public function depends_on()
    {
        return array('\phpbb\db\migration\data\v31x\v314');
    }

    public function update_schema()
    {
        return array(
            'add_tables'		=> array(
                $this->table_prefix . 'rss_items'	=> array(
                    'COLUMNS'		=> array(
                        'rss_id'		=> array('UINT', null),
                        'data'			=> array('MTEXT', ''),
                        'updated'		=> array('TIMESTAMP', ''),
                    ),
                    //'FOREIGN_KEY'	=> 'rss_id',
                ),
            ),
        );
    }

    public function revert_schema()
    {
        return array(
            'drop_tables'		=> array(
                $this->table_prefix . 'rss_items',
            ),
        );
    }
}
