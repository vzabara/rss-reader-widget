## RSS Reader Widget for phpBB ##

The widget allows to show RSS feeds in the bottom of the page.

![ACP](screenshot-3.png)

![ACP](screenshot-2.png)

![Topic](screenshot-1.png)
