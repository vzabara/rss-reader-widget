<?php
/**
 *
 * This file is part of the phpBB Forum Software package.
 *
 * @copyright (c) phpBB Limited <https://www.phpbb.com>
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 * For full copyright and license information, please see
 * the docs/CREDITS.txt file.
 *
 */

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

// BC global FTW
global $table_prefix;

// Table names
define('RSS_TABLE',			$table_prefix . 'rss_sources');
define('RSS_ITEMS_TABLE',	$table_prefix . 'rss_items');
