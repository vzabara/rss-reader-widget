<?php
/**
 *
 * This file is part of the phpBB Forum Software package.
 *
 * @copyright (c) phpBB Limited <https://www.phpbb.com>
 * @license       GNU General Public License, version 2 (GPL-2.0)
 *
 * For full copyright and license information, please see
 * the docs/CREDITS.txt file.
 *
 */

namespace athc\rssreader\event;

/**
 * @ignore
 */
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event listener
 */
class widget_listener implements EventSubscriberInterface
{
    static public function getSubscribedEvents()
    {
        return array(
            'core.user_setup'  => array(array('load_language_on_setup'), array('define_constants')),
            'core.page_header' => 'read_news',
        );
    }

    /* @var \phpbb\controller\helper */
    protected $helper;

    /* @var \phpbb\template\template */
    protected $template;

    /* @var \phpbb\db\driver\driver_interface */
    protected $db;

    /**
     * Constructor
     *
     * @param \phpbb\controller\helper $helper   Controller helper object
     * @param \phpbb\template\template $template Template object
     */
    public function __construct(
        \phpbb\controller\helper $helper,
        \phpbb\template\template $template,
        \phpbb\db\driver\driver_interface $db
    ) {
        $this->helper = $helper;
        $this->template = $template;
        $this->db = $db;
    }

    public function define_constants()
    {
        include_once __DIR__ . '/../includes/constants.php';
    }

    /**
     * Load the Acme Demo language file
     *     acme/demo/language/en/demo.php
     *
     * @param \phpbb\event\data $event The event object
     */
    public function load_language_on_setup($event)
    {
        $lang_set_ext = $event['lang_set_ext'];
        $lang_set_ext[] = array(
            'ext_name' => 'athc/rssreader',
            'lang_set' => 'rssreader',
        );
        $event['lang_set_ext'] = $lang_set_ext;
    }

    /**
     * Show RSS feed
     */
    public function read_news()
    {
        $sql = 'SELECT * FROM ' . RSS_TABLE . ' WHERE active=1';
        $result = $this->db->sql_query($sql);
        $rss = 0;
        while ($row = $this->db->sql_fetchrow($result)) {
            $sql2 = 'SELECT `data` FROM ' . RSS_ITEMS_TABLE . ' WHERE rss_id = ' . $row['id'] . ' AND `updated` > DATE_SUB(NOW(), INTERVAL 5 MINUTE)';
            $result2 = $this->db->sql_query($sql2);
            $xmlStr = '';
            $items = $this->db->sql_fetchrow($result2);
            if (!$items) {
                $xmlStr = file_get_contents($row['url']);
                if ($xmlStr) {
                    $data = array(
                        'rss_id' => $row['id'],
                        'data'   => $xmlStr,
                    );
                    $sql3 = 'DELETE FROM ' . RSS_ITEMS_TABLE . ' WHERE rss_id = ' . $row['id'];
                    $this->db->sql_query($sql3);
                    $sql3 = 'INSERT INTO ' . RSS_ITEMS_TABLE . ' ' . $this->db->sql_build_array('INSERT', $data);
                    $this->db->sql_query($sql3);
                }
            } else {
                $xmlStr = $items['data'];
            }
            if ($xmlStr) {
                $records = $this->parseFeed($xmlStr, $row['num']);
                if (count($records)) {
                    $rss++;
                    $this->template->assign_block_vars('rss', array(
                        'label' => $row['label'],
                    ));
                    foreach ($records as $record) {
                        $this->template->assign_block_vars('rss.items', $record);
                    }
                }
            }
        }
        $this->db->sql_freeresult($result);
        if ($rss) {
            $this->template->assign_var('RSS_FEEDS', true);
        }
    }

    protected function parseFeed($data, $counter = 5)
    {
        $rss = simplexml_load_string($data);
        $count = 0;
        $items = array();
        foreach ($rss->channel->item as $item) {
            $count++;
            if ($count > $counter) {
                break;
            }
            $items[] = array(
                'url'   => (string)$item->link,
                'title' => (string)$item->title,
                'date'  => (string)$item->pubDate,
            );
        }
        return $items;
    }

}
