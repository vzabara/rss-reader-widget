<?php

namespace athc\rssreader\acp;

class widget_info
{
	public function module()
	{
		return array(
			'filename'  => '\athc\rssreader\acp\widget_module',
			'title'     => 'ACP_RSSREADER_TITLE',
			'modes'    => array(
				'settings'  => array(
					'title' => 'ACP_RSSREADER_SETTINGS',
					'auth'  => 'ext_athc/rssreader && acl_a_board',
					'cat'   => array('ACP_RSSREADER_TITLE'),
				),
			),
		);
	}
}