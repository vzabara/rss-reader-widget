<?php

namespace athc\rssreader\acp;

class widget_module
{
    var $u_action;

    function main($id, $mode)
    {
        global $db, $user, $template, $phpbb_log, $request;

        //$user->add_lang('rssreader');

        // Set up general vars
        $action = $request->variable('action', '');
        $action = (isset($_POST['add'])) ? 'add' : ((isset($_POST['save'])) ? 'save' : $action);

        $s_hidden_fields = '';
        $widget_info = array();

        $this->tpl_name = 'acp_rssreaderwidget_body';
        $this->page_title = 'ACP_RSSREADER_TITLE';

        $form_name = 'athc_rssreader_settings';
        add_form_key($form_name);

        switch ($action) {
            case 'edit':

                $widget_id = $request->variable('id', 0);

                if (!$widget_id) {
                    trigger_error($user->lang['NO_WIDGET'] . adm_back_link($this->u_action), E_USER_WARNING);
                }

                $sql = 'SELECT *
					FROM ' . RSS_TABLE . ' 
					WHERE id = ' . $widget_id;
                $result = $db->sql_query($sql);
                $widget_info = $db->sql_fetchrow($result);
                $db->sql_freeresult($result);

                $s_hidden_fields .= '<input type="hidden" name="id" value="' . $widget_id . '" />';

            case 'add':

                $template->assign_vars(array(
                        'S_EDIT_WORD'     => true,
                        'U_ACTION'        => $this->u_action,
                        'U_BACK'          => $this->u_action,
                        'URL'             => (isset($widget_info['url'])) ? $widget_info['url'] : '',
                        'LABEL'           => (isset($widget_info['label'])) ? $widget_info['label'] : '',
                        'NUM'             => (isset($widget_info['num'])) ? $widget_info['num'] : 5,
                        'ACTIVE'          => (isset($widget_info['active'])) ? $widget_info['active'] : 0,
                        'S_HIDDEN_FIELDS' => $s_hidden_fields,
                    )
                );
                return;

            break;

            case 'save':

                if (!check_form_key($form_name)) {
                    trigger_error($user->lang['FORM_INVALID'] . adm_back_link($this->u_action), E_USER_WARNING);
                }

                $widget_id = $request->variable('id', 0);
                $url = $request->variable('url', '', true);
                $label = $request->variable('label', '', true);
                $num = $request->variable('num', 5);
                $active = $request->variable('active', '', true);

                if ($url === '' || $label === '') {
                    trigger_error($user->lang['ENTER_DATA'] . adm_back_link($this->u_action), E_USER_WARNING);
                }

                $sql_ary = array(
                    'url'    => $url,
                    'label'  => $label,
                    'num'    => $num,
                    'active' => $active ? 1 : 0,
                );

                if ($widget_id) {
                    $db->sql_query('UPDATE ' . RSS_TABLE . ' SET ' . $db->sql_build_array('UPDATE',
                            $sql_ary) . ' WHERE id = ' . $widget_id);
                } else {
                    $db->sql_query('INSERT INTO ' . RSS_TABLE . ' ' . $db->sql_build_array('INSERT',
                            $sql_ary));
                }

                $log_action = ($widget_id) ? 'LOG_RSS_WIDGET_EDIT' : 'LOG_RSS_WIDGET_ADD';

                $phpbb_log->add('admin', $user->data['user_id'], $user->ip, $log_action, false, array($widget_id));

                $message = ($widget_id) ? $user->lang['WIDGET_UPDATED'] : $user->lang['WIDGET_ADDED'];
                trigger_error($message . adm_back_link($this->u_action));

            break;

            case 'delete':

                $widget_id = $request->variable('id', 0);

                if (!$widget_id) {
                    trigger_error($user->lang['NO_WIDGET'] . adm_back_link($this->u_action), E_USER_WARNING);
                }

                if (confirm_box(true)) {
                    $sql = 'DELETE FROM ' . RSS_TABLE . "
						WHERE id = $widget_id";
                    $db->sql_query($sql);

                    $phpbb_log->add('admin', $user->data['user_id'], $user->ip, 'LOG_COIN_WIDGET_DELETE', false,
                        array($widget_id));

                    trigger_error($user->lang['WIDGET_REMOVED'] . adm_back_link($this->u_action));
                } else {
                    confirm_box(false, $user->lang['CONFIRM_OPERATION'], build_hidden_fields(array(
                        'i'      => $id,
                        'mode'   => $mode,
                        'id'     => $widget_id,
                        'action' => 'delete',
                    )));
                }

            break;
        }

        $template->assign_vars(array(
                'U_ACTION'        => $this->u_action,
                'S_HIDDEN_FIELDS' => $s_hidden_fields,
            )
        );


        $sql = 'SELECT * FROM ' . RSS_TABLE;
        $result = $db->sql_query($sql);

        while ($row = $db->sql_fetchrow($result)) {
            $template->assign_block_vars('widgets', array(
                    'URL'      => $row['url'],
                    'LABEL'    => $row['label'],
                    'NUM'      => $row['num'],
                    'ACTIVE'   => $row['active'] == 1 ? true : false,
                    'U_EDIT'   => $this->u_action . '&amp;action=edit&amp;id=' . $row['id'],
                    'U_DELETE' => $this->u_action . '&amp;action=delete&amp;id=' . $row['id'],
                )
            );
        }
        $db->sql_freeresult($result);
    }
}