<?php

if (!defined('IN_PHPBB')) {
    exit;
}

if (empty($lang) || !is_array($lang)) {
    $lang = array();
}

$lang = array_merge($lang, array(
    'RSSREADER_PAGE'         => 'RSS Reader Widget',
    'ACP_RSSREADER_TITLE'    => 'RSS Reader Widget',
    'ACP_RSSREADER_EXPLAIN'  => '',
    'ACP_RSSREADER_WIDGETS'  => 'RSS Reader Widgets',
    'ACP_RSSREADER_SETTINGS' => 'RSS Reader Widget',
    'ACP_RSSREADER_SAVED'    => 'Record have been saved successfully!',
    'EDIT_WIDGET'            => 'Edit Widget',
    'ADD_WIDGET'             => 'Add Widget',
    'NO_WIDGET'              => 'RSS Reader widget was not found',
    'ACP_NO_WIDGETS'         => 'RSS Reader widget was not found',
    'FORM_INVALID'           => 'Something wrong with form data',
    'ENTER_DATA'             => 'Data was not set',
    'WIDGET_UPDATED'         => 'RSS Reader widget was updated',
    'WIDGET_ADDED'           => 'RSS Reader widget was added',
    'WIDGET_REMOVED'         => 'RSS Reader widget was removed',
    'URL_TITLE'              => 'RSS URL',
    'LABEL_TITLE'            => 'Label',
    'NUM_TITLE'              => 'Number of items to show',
    'ACTIVE_TITLE'           => 'Active',
));